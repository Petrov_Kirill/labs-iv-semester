#include <iostream>
#include <fstream>
#include <map>
#include <deque>
#include <vector>
#include <string>
#include <locale>
#include <ctime>

using namespace std;

const int N = 1;// ����� ��������
typedef deque<string> Prefix;

map <Prefix, vector<string> > db;

void add(Prefix &p, string s)
{
	if (p.size() == N)
	{
		db[p].push_back(s);
		p.pop_front();
	}
	p.push_back(s);
}

void build(Prefix &p, ifstream & in)
{
	string buf;
	while (in >> buf)
		add(p, buf);
}

void generate(int nwords)
{
	
	ifstream Input("input.txt");
	Prefix P;
	string buf;
	Input >> buf;
	Input.close();
	P.push_back(buf);//first word in file
	for (int i = 0; i < nwords; i++)
	{
		vector <string>& suf = db[P];
		if (suf.empty())
			continue;
		string s = suf[rand() % suf.size()];
		cout << s << " ";
		P.pop_front();
		P.push_back(s);
	}
}

int main()
{
	setlocale(LC_ALL, "Russian");
	ifstream Input ("input.txt");
	Prefix P;

	build(P, Input);
	map <Prefix, vector<string> >::const_iterator iter;
	Input.close();

	srand(time(NULL));
	generate(80);
}