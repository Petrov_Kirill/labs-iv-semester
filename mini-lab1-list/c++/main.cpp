#include "LList.h"
#include <iostream>
#include <fstream>

using namespace std;

DataType ToStructure(char *str)
{
	DataType temp;
	int i = 0;
	char buff[MaxBuff];

	while (*str != ',')
		buff[i++] = *str++;
	buff[i] = 0;
	strcpy_s(temp.UNCODE,buff);
	memset(buff, 0, MaxBuff);
	i = 0;
	str++;

	while (*str != ',')
		buff[i++] = *str++;
	buff[i] = 0;
	strcpy_s(temp.ISO2, buff);
	memset(buff, 0, MaxBuff);
	i = 0;
	str++;

	while (*str != ',')
		buff[i++] = *str++;
	buff[i] = 0;
	strcpy_s(temp.ISO3, buff);
	memset(buff, 0, MaxBuff);
	i = 0;
	str++;

	while (*str != ',')
		buff[i++] = *str++;
	buff[i] = 0;
	strcpy_s(temp.Name, buff);
	memset(buff, 0, MaxBuff);
	i = 0;
	str++;

	while ((*str != '\n')&&(*str))
		buff[i++] = *str++;
	buff[i] = 0;
	strcpy_s(temp.Capital, buff);
	memset(buff, 0, MaxBuff);
	i = 0;
	str++;
	return temp;
}


int main()
{
	LList list;
	char buf[MaxBuff];
	ifstream In("countries.csv");

	//���������� ������ ��������
	In.getline(buf, MaxBuff);
	while (In.getline(buf, MaxBuff))
		list.addTail(ToStructure(buf));
	//����� �� �������
	list.searchCapital("Moscow");


	In.close();
}