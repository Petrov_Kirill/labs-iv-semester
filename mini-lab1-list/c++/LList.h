#ifndef _LLIST_H_
#define _LLIST_H_

const int MaxBuff = 256;
const int N = 5;

struct Countries
{
	char UNCODE[N];
	char ISO2[N];
	char ISO3[N];
	char Name[MaxBuff];
	char Capital[MaxBuff];
} typedef DataType;

class LList
{
private:
    struct ITEM
    {
        DataType *data;
        ITEM* next;
    };
    LList::ITEM* create(const DataType&);
    ITEM *head;
    ITEM *tail;
public:
    LList():head(0), tail(0) {};
    LList (const DataType&);
    void addHead(const DataType&);
    void addTail(const DataType&);
	void searchCapital(char *)const;
	void printCapital()const;
	void print()const;
};

#endif _LLIST_H_