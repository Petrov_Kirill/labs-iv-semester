#include "LList.h"
#include <iostream>

using namespace std;

LList::ITEM* LList::create(const DataType &data)
{
    ITEM *item = new ITEM;
    item -> data = new DataType;
    *item-> data = data;
    item -> next = 0;
    return item;
}

LList::LList(const DataType& data)
{
	head = create(data);
	tail = head;
}

void LList::addTail(const DataType& data)
{
    if (head && tail)
    {
		tail->next = create(data);
        tail = tail -> next;
    }
    else 
    {
        head = create(data);
        tail = head;
    }
}

void LList::addHead(const DataType&data)
{
    if (head && tail)
    {
        ITEM *temp = new ITEM;
        temp -> next = head;
        head = temp;
    }
    else
    {
        head = create(data);
        tail = head; 
    }
}

void LList::searchCapital(char *str) const
{
	ITEM* temp = head;
	while (temp)
	{
		if (!strcmp(temp->data->Capital, str))
			cout
			<< temp->data->UNCODE << " | "
			<< temp->data->ISO2 << " | "
			<< temp->data->ISO3 << " | "
			<< temp->data->Name << " | "
			<< temp->data->Capital << endl;
		temp = temp->next;
	}
}

void LList::printCapital() const
{
	ITEM * temp = head;
	while (temp)
	{
		cout<< temp->data->Capital << endl;
		temp = temp->next;
	}
}

void LList::print() const
{
	ITEM * temp = head;
	while (temp)
	{
		cout
		<< temp->data->UNCODE << " | "
		<< temp->data->ISO2 << " | "
		<< temp->data->ISO3 << " | "
		<< temp->data->Name << " | "
		<< temp->data->Capital << endl;
		temp = temp->next;
	}
}