#include <iostream>
#include <fstream>
#include <ctime>
#include <vector>
#include <list>

using namespace std;

const int N=8000;

int main()
{
	vector<char> v;
	list <char> l; 
	ifstream in ("input.txt");
	char buf;
	srand(time(0));

	//filling vect&list
	while (in>>buf)
	{
		l.push_back(buf);
		v.push_back(buf);
	}

	in.close();
	//start time list

	int tl1 = clock();
	for (int i=0; i<N; i++)
		l.push_front(char(rand()%255));
	int tl2 = clock();

	//finish time list
	cout <<"Size file: "<< l.size()<<endl;
	cout << tl2-tl1 << "  " << ((float)(tl2-tl1)) / CLOCKS_PER_SEC << " list"<<endl;
	//start time vect

	int tv1 = clock();
	for (int i=0; i<N; i++)
		v.insert(v.begin(), (char(rand()%255)) );
	int tv2 = clock();

	//finish time vect
	cout << tv2-tv1 << "  " << ((float)(tv2-tv1)) / CLOCKS_PER_SEC << " vector";
}
