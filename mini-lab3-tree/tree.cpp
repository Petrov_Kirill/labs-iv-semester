#include <iostream>
#include <string>
#include <fstream>
#include <locale>

using namespace std;
typedef string DataType;

ifstream in("input.txt");
ofstream out("preliminary.txt");

struct NODE
{
	DataType data;
	NODE *left;
	NODE *right;
	int count;
};

NODE *addNode(const DataType&data, NODE *root)
{
	if (root == NULL)
	{
		root = new NODE;
		root->data = data;
		root->left = NULL;
		root->right = NULL;
		root->count = 1;
	}
	else if (root->data > data)
		root->left = addNode(data, root->left);
	else if (root->data < data)
		root->right = addNode(data, root->right);
	else
		root->count++;
	return root;
}

NODE *Search(const DataType &data, NODE*root)
{
	if (root != 0)
	{
		if (data == root->data)
			return root;
		else if (root->data < data)
			return Search(data, root->right);
		else if (root->data > data)
			return Search(data, root->left);
		else
			return NULL;
	}
}

void PrintFile(NODE *root)
{
	if (root->left)
		PrintFile(root->left);
	out << root->data<<" | "<<root->count<<endl;
	if (root->right)
		PrintFile(root->right);
}

int main()
{
	setlocale(LC_ALL, "rus");
	string buf;
	in >> buf;
	NODE *root=NULL;
	while (in >> buf)
		root=addNode(buf,root);
	PrintFile(root);
	in.close();
	out.close();
}