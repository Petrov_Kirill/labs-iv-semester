
patch = r"preliminary.txt"
infile = open(patch, 'r')
out=open("output.txt",'w')
data = [line for line in infile]
data.sort(key=lambda line: int(line.split()[2]),reverse=True)
out.write("".join(data))
infile.close()
out.close()